---
layout:     post
title:      Sweater Day
date:       2017-02-02 00:00:01
author:     SomeGuy
summary:    Sweater Day is Feb 2nd
categories: blog
thumbnail:  calendar
tags:
 - February
 - SweaterDay
---
Febuary 2 of every year is the official day for sweaters. It is a fun way to learn about the importance of saving energy use. Enjoy your ladies sweater puppies today!

Social media users: #NationalSweaterDay #SweaterDay

Happy [National Sweater Day][1]! 

[1]: http://www.wwf.ca/events/sweater_day/
