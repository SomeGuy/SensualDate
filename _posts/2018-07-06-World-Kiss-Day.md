---
layout:     post
title:      World Kiss Day
date:       2018-07-06 00:00:01
author:     SomeGuy
summary:    World Kiss Day is July 6th
categories: blog
thumbnail:  calendar
tags:
 - July
 - InternationalDay
 - KissingDay
---

International Kissing Day started out as National Kissing Day in the UK but has since spread to World Kiss Day.

Social media users: #NationalKissingDay is used for UK residents, but the rest of the world can use #InternationalKissingDay or #WorldKissDay

Happy International [Kissing][1] [Day][2] everyone!

Happy National [Kissing Day][3], UK! 

[1]: http://www.national-awareness-days.com/international-kissing-day.html
[2]: http://www.cute-calendar.com/event/world-kiss-day/15763.html
[3]: https://www.facebook.com/NationalKissingDay
