---
layout:     post
title:      August 28th is Go Topless Day
date:       2016-08-28 00:00:01
author:     SomeGuy
summary:    August 28th is Go Topless Day
categories: blog
thumbnail:  calendar
tags:
 - August
 - NationalDay
 - GoToplessDay
---

Social media users: #GoTopless

[GoTopless][1] is a group that organizes parades and rallies across the world to support the same equal rights for females to go bare-chested in public that males currently have. More information on where rallies and parades are being held along with information on how you can support the cause can be found on their website.

[1]: http://gotopless.org/
