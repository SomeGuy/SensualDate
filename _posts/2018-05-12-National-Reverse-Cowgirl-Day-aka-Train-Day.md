---
layout:     post
title:      National Reverse Cowgirl Day and Train Day
date:       2018-05-12 00:00:01
author:     SomeGuy
summary:    National Reverse Cowgirl Day is May 12th
categories: blog
thumbnail:  calendar
tags:
 - May
 - NationalDay
 - NationalTrainDay
 - NationalReverseCowgirlDay
---

Social media users: #NationalTrainDay #NationalReverseCowgirlDay #woowoo

The Saturday closest to May 10th is known as [National Train Day][1]. Referencing Quad City DJ's 90's hit ['Come on N Ride it'][2], many have taken to celebrating Train day with Reverse Cowgirl while sitting on a chair.

[1]: https://en.wikipedia.org/wiki/National_Train_Day
[2]: https://en.wikipedia.org/wiki/C%27mon_N%27_Ride_It_%28The_Train%29
