---
layout:     post
title:      September 3rd is National High Heels Day & Skyscraper Day
date:       2017-09-03 00:00:01
author:     SomeGuy
summary:    September 3rd is National High Heels Day and Skyscraper Day
categories: blog
thumbnail:  calendar
tags:
 - September
 - NationalDay
 - HighHeelsDay
 - SkyScraperDay
---

Social media users: #NationalHighHeelsDay #SkyScraperDay

In celebration of National Sky Scraper Day, women show their admiration by wearing  high, high heels and enjoying the sights from the top of a sky scraper near them thereby also desginating it as National [High Heels Day][1] as well.

[1]: http://blog.shoedazzle.com/national-sky-scraper-day-feat-stellar-high-heels/
