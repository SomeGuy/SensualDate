---
layout:     post
title:      National Cowgirl Day
date:       2016-07-23 00:00:01
author:     SomeGuy
summary:    National Cowgirl Day is July 23rd
categories: blog
thumbnail:  calendar
tags:
 - July
 - NationalDay
 - CowgirlDay
---

"Today is a day set aside to celebrate the contributions of the Cowboy and Cowgirl to America’s culture and heritage." - [National Day of the Cowboy Organization][1]. It is celebrated on the fourth Saturday in July.

Social media users: #NationalDayOfTheCowboy #NationalCowgirlDay

[1]: http://nationaldayofthecowboy.com/wordpress/
