---
layout:     post
title:      Husband Appreciation Day
date:       2017-04-15 00:00:01
author:     SomeGuy
summary:    Husband Appreciation Day is 3rd Saturday in April
categories: blog
thumbnail:  calendar
tags:
 - April
 - HusbandAppreciationDay
---

[Husband Appreciation Day][1] is a time where wives show their appreciation for their husband. Originally celebrated by those without children who are not included in Father's Day activities, it is now encouraged for all wives to participate. 

Wife Appreciation Day is Third Sunday in September.

Social media users: #HusbandAppreciationDay

[1]: http://holidayinsights.com/moreholidays/April/husband-appreciation-day.htm
