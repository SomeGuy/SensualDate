---
layout:     post
title:      Victoria's Secret presents International Bombshell's Day
date:       2017-05-06 00:00:02
author:     SomeGuy
summary:    First Saturday in May
categories: blog
thumbnail:  calendar
tags:
 - May
 - InternationalDay
 - InternationalBombshellsDay
---

Social media users: #InternationalBombshellsDay

Victoria's Secret declared in 2015 that the first Saturday in May would be [International Bombshell's Day][1].

Enjoy your Lingerie Today!!

[1]: http://vspressroom.com/beauty/bombshells-day-2015/
