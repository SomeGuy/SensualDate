---
layout:     post
title:      August 26th is National Doggy Style Day
date:       2018-08-26 00:00:01
author:     SomeGuy
summary:    August 26th is National Doggy Style Day
categories: blog
thumbnail:  calendar
tags:
 - August
 - NationalDay
 - DoggyStyleDay
---

Social media users: #NationalDogDay #NationalDoggyStyleDay

Not much was found for a source on the origins on this day, however, as it is also National Dog Day it is most likely just an alt celebration of a similarly themed activity.
