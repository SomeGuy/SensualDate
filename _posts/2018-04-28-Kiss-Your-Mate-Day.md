---
layout:     post
title:      Kiss Your Mate Day
date:       2018-04-28 00:00:02
author:     SomeGuy
summary:    Kiss Your Mate Day is April 28th
categories: blog
thumbnail:  calendar
tags:
 - April
 - KissYourMateDay
---

With spring in the air, April is the perfect chance to smooch with your partner on [Kiss Your Mate Day][1]!

Social media users: #KissYourMateDay

[1]: http://eventguide.com/d/100822.htm
