---
layout:     post
title:      National Miniskirt Day
date:       2018-05-11 00:00:01
author:     SomeGuy
summary:    National Miniskirt Day is May 11th
categories: blog
thumbnail:  calendar
tags:
 - May
 - NationalDay
 - NationalMiniskirtDay
---

Social media users: #2ndFriMayMiniskirtDay #NationalMiniskirtDay

According to this [twitter account][1] May 11th 2018, being the second Friday in May, is National Miniskirt Day. It is promoted as the day for all those who have worked hard at the Gym to show off the results of their body and legs in their favorite miniskirt. 

[1]: https://twitter.com/MiniskirtDay
