---
layout:     post
title:      National Skirt Day
date:       2017-03-10 00:00:01
author:     SomeGuy
summary:    National Skirt Day is Mar 10th
categories: blog
thumbnail:  calendar
tags:
 - March
 - NationalDay
 - SkirtDay
---
In 2011, a twitter account and web page launched announcing [National Skirt Day][1] to be March 10, close to the start of Spring. While the site had some impact for a few years, they stopped being active in 2013 leading many to celebrate national skirt day at various times in the year.

Social media users: #NationalSkirtDay #ShowOffYourLegsDay

Happy National Skirt Day! 

[1]: http://nationalskirtday.com
