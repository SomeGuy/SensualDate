---
layout:     post
title:      National Lingerie Day
date:       2017-04-24 00:00:01
author:     SomeGuy
summary:    National Lingerie Day is April 24th
categories: blog
thumbnail:  calendar
tags:
 - April
 - NationalLingerieDay
 - NationalDay
---

[National Lingerie Day][1] was started to help women learn about how to buy properly fitted lingerie. They wanted to answer questions women had about bra fitting, choosing a style, when to use a garter belt, and many more. They want to give women an outlet for lingerie to be a form of self-expression after giving them information to make the best choices. Learn about lingerie today. Find a store near you that in participating in giving women good advice and information about lingerie.

Social media users: #NationalLingerieDay

[1]: http://www.thelingerieaddict.com/2012/04/national-lingerie-day-why-wear-lingerie.html 
