---
layout:     post
title:      National Marriage Day
date:       2018-06-08 00:00:01
author:     SomeGuy
summary:    National Marriage is Second Friday in June
categories: blog
thumbnail:  calendar
tags:
 - May
 - NationalDay
 - NationalMarriageDay
---

Social media users: #NationalMarriageDay

[National Marriage Day][1] is a day celebrate your marriage by recommitting to your vows and cherishing your spouse.

National Marriage Day is celebrated the second Friday in June.

[1]: http://eventguide.com/d/104715.htm