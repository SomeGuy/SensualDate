---
layout:     post
title:      International Orgasm Day
date:       2018-07-31 00:00:01
author:     SomeGuy
summary:    International Orgasm Day is July 31rst
categories: blog
thumbnail:  calendar
tags:
 - July
 - InternationalDay
 - OrgasmDay
---

Going back to at least [2013][2], the UK has celebrated National Orgasm Day to promote sexual health. In recent years, [many][1] have begun celebrating it around the world allowing it to be considered an international "Orgasm Day". 

Social media users: #NationalOrgasmDay #OrgasmDay

[1]: http://www.all-sweetness-and-life.com/sex/its-national-orgasm-day-and-heres-everything-you-need-to-know-about-our-favourite-thing-to-do-in-the-bedroom/
[2]: http://www.smh.com.au/lifestyle/life/citykat/o-wow--what-a-day-to-celebrate-20130730-2qxcn
