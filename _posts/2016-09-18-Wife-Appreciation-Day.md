---
layout:     post
title:      Third Sunday in September is Wife Appreciation Day
date:       2016-09-18 00:00:01
author:     SomeGuy
summary:    Third Sunday in September is Wife Appreciation Day
categories: blog
thumbnail:  calendar
tags:
 - September
 - NationalDay
 - WifeAppreciationDay
---

Social media users: #WifeAppreciationDay

[Wife Appreciation Day][1] is the third Sunday in September. It is an opportunity for a husband to tell his wife how much he appreciates all the things she does, both big and small.

[1]: http://www.nationaldaycalendar.com/wife-appreciation-day-third-sunday-in-september/
