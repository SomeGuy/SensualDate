---
layout:     post
title:      National Tell a girl shes beautiful day
date:       2018-07-19 00:00:01
author:     SomeGuy
summary:    National Tell a girl shes beautiful Day is July 19th
categories: blog
thumbnail:  calendar
tags:
 - July
 - NationalDay
 - TellAGirlShesBeautifulDay
---

[Tell your girl she's beautiful today][1]! This holiday started in 2013 and while we should all tell our wives this daily, it is a good encouragement to start today.

Social media users: #TellAGirlShesBeautifulDay


[1]: https://www.eventguide.com/d/104711.htm
