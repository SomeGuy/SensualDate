---
layout:     post
title:      National Cleavage Day
date:       2018-03-31 00:00:01
author:     SomeGuy
summary:    National Cleavage Day is Mar 30th
categories: blog
thumbnail:  calendar
tags:
 - March
 - NationalDay
 - CleavageDay
---
[National Cleavage Day][1] was started by Wonderbra in 2002 as "a day for women to realise that their cleavage is something unique and that they should be proud of it" and "a way to solemnise women's independence and power in all facets of life." It was intended to be a lighthearted amusement to raise funds for charity. More information can be found on the [website][2].

Social media users: #NationalCleavageDay

Happy National Cleavage Day! 

[1]: https://en.wikipedia.org/wiki/National_Cleavage_Day
[2]: http://nationalcleavageday.com/
