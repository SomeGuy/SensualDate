---
layout:     post
title:      National Cowgirl Day
date:       2018-07-28 00:00:01
author:     SomeGuy
summary:    National Cowgirl Day is July 28th
categories: blog
thumbnail:  calendar
tags:
 - July
 - NationalDay
 - CowgirlDay
---

"Today is a day set aside to celebrate the contributions of the Cowboy and Cowgirl to America’s culture and heritage." - [National Day of the Cowboy Organization][1]. It is celebrated on the fourth Saturday in July.

Celebrate by riding [Cowgirl][2]!

Social media users: #NationalDayOfTheCowboy #NationalCowgirlDay

[1]: http://nationaldayofthecowboy.com/wordpress/
[2]: https://www.christianfriendlysexpositions.com/cowgirl/
