---
layout:     post
title:      National Stripper/Striptease Day and World 69 Day
date:       2018-06-09 00:00:01
author:     SomeGuy
summary:    National Stripper Day and World 69 Day are June 9th
categories: blog
thumbnail:  calendar
tags:
 - June
 - NationalDay
 - WorldDay
 - NationalStripperDay
 - World69Day
---

National Stripper Day aka National Striptease Day
Social media users: #NationalStripperDay #NationalStripteaseDay

We found a lot of references people made to this day being National Stripper Day celebrating the dedication of Strippers, or in some cases when everyone is supposed to plan a trip to their local strip club. But we also found several references to this day being the day when YOU are supposed to plan a strip tease for that special one in your life.

The only real link we found for declaring the day was this [petition][1] for a National Stripper Appreciation Day.

World 69 Day
Social media users: #World69day #NationalSexDay or just #69

This day is also a pun. July being the 6th month and the day being the 9th = 6/9. It has been declared World 69 day by many though there is some challenge since many countries reverse month and day making 6th of September 6/9 day. It has a dedicated [facebook][2] page.


[1]: http://www.thepetitionsite.com/1/national-stripper-appreciation-day/
[2]: https://www.facebook.com/June-9th-National-Sex-Day-69-214840458537241
