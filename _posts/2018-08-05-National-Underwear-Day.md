---
layout:     post
title:      August 5th is National Underwear Day
date:       2018-08-05 00:00:01
author:     SomeGuy
summary:    August 5th is National Underwear Day
categories: blog
thumbnail:  calendar
tags:
 - August
 - NationalDay
 - Underwear
---

Social media users: #NationalUnderwearDay

According to [nationaldaycalendar.com][1], Freshpair founded National Underwear Day on August 5th, 2003. Wear your favorite pair today!

[1]: http://www.nationaldaycalendar.com/national-underwear-day-august-5/
