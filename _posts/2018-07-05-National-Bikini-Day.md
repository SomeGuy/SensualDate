---
layout:     post
title:      National Bikini Day
date:       2018-07-05 00:00:01
author:     SomeGuy
summary:    National Bikini Day is July 5th
categories: blog
thumbnail:  calendar
tags:
 - July
 - NationalDay
 - BikiniDay
---

National Bikini Day
Social media users: #BikiniDay or #NationalBikiniDay 

According to the nationaldaycalendar.com July 5th has been [Bikini Day][1] since 1946! It marks the anniversary of the invention of this bathing suit.

[1]: http://www.nationaldaycalendar.com/national-bikini-day-july-5/
