---
layout:     post
title:      National No Bra Day
date:       2018-10-13 00:00:01
author:     SomeGuy
summary:    National No Bra Day is Oct 13th
categories: blog
thumbnail:  calendar
tags:
 - August
 - NationalDay
 - NoBraDay
---

National No Bra Day appears to be celebrated multiple times in the year. October 13th No Bra Day is in support of the very serious matter of breast cancer awareness. Unlike the July 9th 'No Bra Day' celebration of womens breasts that encourages them to be free, Oct 13th encourages women to check for lumps and encourages everyone to support the cause. October is Breast Cancer Awareness Month. Wear pink!

Social media users: #NationalNoBraDay 

Happy National [No Bra Day][1]! 

[1]: www.nationaldaycalendar.com/national-no-bra-day/
