---
layout:     post
title:      Good Knee High Socks Day
date:       2016-11-28 00:00:01
author:     SomeGuy
summary:    Good Knee High Socks Day is Nov 28th
categories: blog
thumbnail:  calendar
tags:
 - November
 - GoodKneeHighSocksDay
---

Kanji characters form the Japanese language. As they are more of a symbol structure than an alphabet, they lend themselves to different kinds of word play. Today, 11/28 can be translated like this: 1 = l, 11 -> li (Good), 2 = Fu, 8 = Ha(i), 28 -> FuHai (Good Knee Highs). Just a slight change in the word play means that 1122 can be thought of as "Good Knee High Socks Day".

Social media users: #GoodKneeHighSocksDay

Happy [Good Knee High Socks Day][1]! 

[1]: http://knowyourmeme.com/memes/events/good-knee-high-socks-day
