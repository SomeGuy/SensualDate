---
layout:     post
title:      Christmas Eve Quickie
date:       2017-12-24 00:00:01
author:     SomeGuy
summary:    Christmas Eve Quickie is Dec 24th
categories: blog
thumbnail:  calendar
tags:
 - December
 - ChristmasEveQuickie
---
The Marriage Bed has promoted the [Christmas Eve Quickie][1] as an opportunity for married couples to spend a few minutes alone with each other during this busy and hectic season. 2017 is their 6th year.

Social media users: #ChristmasEveQuickie

[1]: https://twitter.com/themarriagebed/status/944885552851161088
