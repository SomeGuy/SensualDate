---
layout:     post
title:      August 17th is National Head Day
date:       2016-08-17 00:00:01
author:     SomeGuy
summary:    August 17th is National Head Day
categories: blog
thumbnail:  calendar
tags:
 - August
 - NationalDay
 - HeadDay
---

Social media users: #NationalHeadDay

Valentines Day is a great day for couples, but it really focuses on what the woman wants. Of course, there is nothing wrong with that--we love women and enjoy treating them well. What was missing was a Holiday that was focused on what men want. So a holiday was created for men to get [head][1].


[1]: http://august17.org/
