---
layout:     post
title:      Restaurant Day
date:       2018-05-19 00:00:01
author:     SomeGuy
summary:    Restaurant Day is May 19th
categories: blog
thumbnail:  calendar
tags:
 - May
 - NationalDay
 - RestaurantDay
---

Social media users: #RestaurantDay

The idea behind [Restaurant Day][1] is to have fun, share new food experiences and enjoy our common living environments together. Celebrate with a [Restaurant Attendant][2] quickie.

Restaurant Day is celebrated the third Saturday in May.

[1]: http://www.restaurantday.org/en/
[2]: http://www.christianfriendlysexpositions.com/restaurant-attendant/
