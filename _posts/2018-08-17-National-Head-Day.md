---
layout:     post
title:      August 17th is National Head Day
date:       2018-08-17 00:00:01
author:     SomeGuy
summary:    August 17th is National Head Day
categories: blog
thumbnail:  calendar
tags:
 - August
 - NationalDay
 - HeadDay
---

Social media users: #NationalHeadDay

Valentines Day is a great day for couples, but it really focuses on what the woman wants. Of course, there is nothing wrong with that--we love women and enjoy treating them well. What was missing was a Holiday that was focused on what men want. In 2011, a holiday was created for men to get [head][1].

The domain august17.org was the focus point until 2017. It moved to facebook [facebook][2] in 2017 but seems to have gone soft since.


[1]: http://august17.org/
[2]: https://www.facebook.com/pages/National-Head-Day/141126502608099
