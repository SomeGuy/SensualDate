---
layout:     post
title:      Lovers Day
date:       2018-04-23 00:00:01
author:     SomeGuy
summary:    Lovers Day is April 23rd
categories: blog
thumbnail:  calendar
tags:
 - April
 - LoversDay
---

While the exact origins of [Lovers Day][1] are unknown, it has been around for a very long time. It is just a day to remind the one you love why you love them. Spend time with them and cherish them on this day!

Social media users: #LoversDay

[1]: http://holidayinsights.com/moreholidays/April/loversday.htm
