---
layout:     post
title:      National Dance Day
date:       2018-07-28 00:00:02
author:     SomeGuy
summary:    National Dance Day is July 28th
categories: blog
thumbnail:  calendar
tags:
 - July
 - NationalDay
 - DanceDay
---

[National Dance Day][1] was started to promote dance education and physical fitness. It is the last Saturday of July. Put on some music and dance with your spouse today.

Social media users: #DanceDay #NationalDanceDay

[1]: https://en.wikipedia.org/wiki/National_Dance_Day
