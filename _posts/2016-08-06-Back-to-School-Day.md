---
layout:     post
title:      August 6th is Back To School-Girl Day
date:       2016-08-06 00:00:01
author:     SomeGuy
summary:    August 6th is Back To School-Girl Day
categories: blog
thumbnail:  calendar
tags:
 - August
 - NationalDay
 - SchoolGirl
---

Social media users: #BackToSchoolDay #SchoolGirl #Costume

Back To School Tax Free Shopping happens every year in August on a Saturday/Sunday weekend (Fridays for some states). Many have taken this to be the weekend celebration of School Girl Costumes starting the Saturday of the event.

