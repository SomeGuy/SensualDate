---
layout:     post
title:      National Nude Day
date:       2017-07-14 00:00:01
author:     SomeGuy
summary:    National Nude Day is July 14th
categories: blog
thumbnail:  calendar
tags:
 - July
 - NationalDay
 - NudeDay
---

National Nude Day - when laundry is unnecessary. Don't break any laws while enjoying your freedom from clothing!

Social media users: #NationalNudeDay 

Happy [National Nude Day][1]! 

[1]: http://nationaldaycalendar.com/national-nude-day-july-14
