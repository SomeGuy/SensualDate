---
layout:     post
title:      The Beginning
date:       2016-05-05 00:00:01
author:     SomeGuy
summary:    A simple social media post that turned into a debate.
categories: blog
thumbnail:  heart
tags:
 - Beginning
---

Welcome to Sensual Dates.

I watched as a social media post from a friend turned into a heated debate on what special 'holiday' it was. Turns out, both sides were completely wrong when we all hit the internet for answers. We found the original post declaring the 'holiday' as well as a website dedicated to it. Many of the social media postings just take a few days to really get going and the highest trending tag may not be on the actual date. For example, we all know that Christmas Day is on December 25th, but if all you saw was trending posts it could be assumed that Christmas Day was at any point between December 14th and 29th.

Thus this project was born. Attempting to tie the actual declared day with some sort of 'official' post or website so everyone will know the actual date.

As this project started to get information from other people it became a way to track days AND to plan a few fun dates with your significant other. Several people have plans underway for upcoming dates.

So check the [wiki][1] for all of the dates. Submit issues for dates and sources (or corrections). And enjoy!

[1]: https://gitlab.com/SomeGuy/SensualDate/wikis/home
