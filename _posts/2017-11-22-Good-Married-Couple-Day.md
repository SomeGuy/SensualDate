---
layout:     post
title:      Good Married Couple Day
date:       2017-11-22 00:00:01
author:     SomeGuy
summary:    Good Married Couple Day is Nov 22nd
categories: blog
thumbnail:  calendar
tags:
 - November
 - GoodMarriedCoupleDay
---

Kanji characters form the Japanese language. As they are more of a symbol structure than an alphabet, they lend themselves to different kinds of word play. Today, 11/22 can be translated like this: 1 = l, 11 -> li (Good), 2 = Fu, 22 -> Fufu (Married Couple). Just a slight change in the word play means that 1122 can be thought of as "Good Married Couple".

Social media users: #GoodMarriedCoupleDay

Happy [Good Married Couple Day][1]! 

[1]: http://knowyourmeme.com/memes/events/good-knee-high-socks-day
