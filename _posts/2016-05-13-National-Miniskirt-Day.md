---
layout:     post
title:      National Miniskirt Day
date:       2016-05-13 00:00:01
author:     SomeGuy
summary:    National Miniskirt Day is May 13th
categories: blog
thumbnail:  calendar
tags:
 - May
 - NationalDay
 - NationalMiniskirtDay
---

Social media users: #2ndFriMayMiniskirtDay #NationalMiniskirtDay

According to this [twitter account][1] May 13th 2016, being the second Friday in May, is National Miniskirt Day. It is promoted as the day for all those who have worked hard at the Gym to show off the results of their body and legs in their favorite miniskirt. 

[1]: https://twitter.com/MiniskirtDay